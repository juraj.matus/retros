# Retros

A simple web application for conducting retrospectives

## Scripts

```sh
sh scripts/build.sh # Builds all necessary Docker images
sh scripts/publish.sh # Publishes the Docker images and generates `docker/docker-compose-deploy.yml`
docker-compose -f docker/docker-compose-deploy.yml [cmd] [...args] # Docker compose then controls the deployment
```

## First run

It's recommended to setup authentication for MongoDB at the first run.
You have to run the project with authentication disabled:

```diff
--- a/docker/docker-compose-deploy.yml
+++ b/docker/docker-compose-deploy.yml
@@ -30,5 +30,4 @@ services:
   mongo:
     container_name: retros-mongo-db
     image: mongo:5.0.5
-    command: [--auth]
     restart: always
```

Then you have to create an account:

```sh
docker-compose -f docker/docker-compose-deploy.yml -p retros exec mongo mongosh
> use retros
> db.createUser({ user: 'retros-user', pwd: 'gP3tLXte9Q7rgixB', roles: [{ role: 'readWrite', db: 'retros'}] })
```