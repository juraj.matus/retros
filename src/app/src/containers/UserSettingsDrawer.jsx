import { Button, Drawer, Spin, Typography } from 'antd';
import axios from 'axios';
import { FormattedMessage } from 'react-intl';
import { useMutation } from 'react-query';
import { useLocation } from 'react-router';
import URI from 'urijs';

import apiRoutes from '../app/apiRoutes';
import { useUserSettings } from '../app/hooks/authHooks';
import { queryClient } from '../appSingletons';
import { axiosToQueryFn } from '../common/utils/httpUtils';

const fetchSetTrelloToken = axiosToQueryFn(() =>
  axios.put(apiRoutes.setTrelloToken, { token: null })
);

function TrelloSection({ enabled }) {
  const { pathname } = useLocation();
  const { mutate, isLoading } = useMutation({
    mutationFn: fetchSetTrelloToken,
    onSuccess: () => {
      queryClient.refetchQueries('userSettings');
    },
  });

  return (
    <>
      <Typography.Title level={3}>
        <FormattedMessage id="userSettings.trello.title" />
      </Typography.Title>
      {enabled ? (
        <Button onClick={mutate} disabled={isLoading} loading={isLoading}>
          <FormattedMessage id="userSettings.trello.disconnect" />
        </Button>
      ) : (
        <Button
          href={URI(apiRoutes.trelloAuthUrl)
            .query({ redirect: pathname })
            .toString()}
        >
          <FormattedMessage id="userSettings.trello.connect" />
        </Button>
      )}
    </>
  );
}

function Content({ userSettings }) {
  return (
    <>
      <TrelloSection enabled={userSettings?.trelloEnabled} />
    </>
  );
}

export default function UserSettingsDrawer({ open, onClose }) {
  const { data, isLoading } = useUserSettings();

  return (
    <Drawer
      open={open}
      onClose={onClose}
      destroyOnClose
      maskClosable
      title={<FormattedMessage id="userSettings.title" />}
      width={350}
    >
      {isLoading && <Spin />}
      {data && <Content userSettings={data} />}
    </Drawer>
  );
}
