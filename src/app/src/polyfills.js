/* eslint-disable no-underscore-dangle */
/* eslint-disable import/no-extraneous-dependencies */

if (process.env.NODE_ENV !== 'production') {
  const { PolyfillChecker, createReactAppPreset } = require('polyfill-checker');

  const checker = new PolyfillChecker({
    minBrowsers: {
      chrome: '60',
      chrome_android: '60',
      edge: '12',
      edge_mobile: false,
      firefox: '54',
      firefox_android: '54',
      ie: false,
      opera: false,
      opera_android: false,
      safari: '10',
      safari_ios: '10',
      webview_android: false,
    },
    exclude: [
      createReactAppPreset,
      // used in: react-dom.development.js
      'WeakMap',
      name => name.indexOf('Set') !== 0,
      name => name.indexOf('Map') !== 0,
      // used in: React DevTools extension
      'String.prototype.codePointAt',
      'Number.isInteger',
    ],
  });
  checker.downgradeMode();
}

export async function loadPolyfills() {
  let IntlPolyfill;
  if (!window.Intl) {
    IntlPolyfill = require('intl');
    window.Intl = IntlPolyfill;
  }
  if (!window.Promise) {
    require('core-js/es/promise');
  }
  if (!window.Set) {
    require('core-js/es/set');
  }
  if (!window.Map) {
    require('core-js/es/map');
  }
  if (!window.Intl.NumberFormat || !window.Intl.DateTimeFormat) {
    IntlPolyfill = IntlPolyfill || require('intl');
    window.Intl.NumberFormat = IntlPolyfill.NumberFormat;
    window.Intl.DateTimeFormat = IntlPolyfill.DateTimeFormat;
  }
  if (IntlPolyfill) {
    // https://github.com/andyearnshaw/Intl.js/issues/231#issuecomment-291049802
    IntlPolyfill.__disableRegExpRestore();
    require('intl/locale-data/jsonp/en');
  }
}
