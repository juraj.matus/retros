/* eslint-disable no-underscore-dangle */
import { identity } from 'lodash-es';
import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import thunk from 'redux-thunk';

import { authReducer } from './redux/auth';
import { dialogsReducer } from './redux/dialogs';
import { settingsReducer } from './redux/settings';

export function makeStore(queryClient) {
  const store = createStore(
    combineReducers({
      auth: authReducer,
      settings: settingsReducer,
      dialogs: dialogsReducer,
    }),
    {},
    compose(
      applyMiddleware(thunk.withExtraArgument({ queryClient })),
      window.__REDUX_DEVTOOLS_EXTENSION__
        ? window.__REDUX_DEVTOOLS_EXTENSION__()
        : identity
    )
  );
  return store;
}
