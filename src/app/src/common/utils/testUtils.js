import { createIntl } from 'react-intl';

import appEn from '../../config/intl/en.json';

export function getTestIntl() {
  return createIntl({ locale: 'en-US', messages: appEn });
}
