import { simpleAction } from './reduxUtils';

describe('simpleAction', () => {
  it('should create no-arg action if no names are passed', () => {
    const action = simpleAction('type1');
    expect(action()).toEqual({ type: 'type1' });
  });
  it('should create one-arg action if one name are passed', () => {
    const action = simpleAction('type2', 'name');
    expect(action('John')).toEqual({ type: 'type2', name: 'John' });
  });
  it('should create multi-arg action if multiple names are passed', () => {
    const action = simpleAction('type3', 'name', 'surname', 'job');
    expect(action('John', 'Doe', 'programmer')).toEqual({
      type: 'type3',
      name: 'John',
      surname: 'Doe',
      job: 'programmer',
    });
  });
});
