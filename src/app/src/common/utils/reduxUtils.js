import { zipObject } from 'lodash-es';

export function simpleAction(type, ...valueKeys) {
  return (...args) => ({
    type,
    ...zipObject(valueKeys, args.slice(0, valueKeys.length)),
  });
}
