const { formatError } = require('./errorUtils');

describe('formatError', () => {
  it('should support standard errors', () => {
    expect(formatError(new Error('Test message'))).toEqual('Test message');
  });
});
