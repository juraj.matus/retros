import axios from 'axios';

export function readMdModule(module) {
  return axios.get(module.default).then(res => res.data);
}
