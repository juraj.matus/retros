import { cssTransitionClassNames } from './reactTransitionUtils';

describe('cssTransitionClassNames', () => {
  it('should return the full classNames object if defaults are used', () => {
    expect(cssTransitionClassNames('fade')).toEqual({
      appear: 'fade-appear',
      appearActive: 'fade-appear-active',
      appearDone: 'fade-appear-done',
      enter: 'fade-enter',
      enterActive: 'fade-enter-active',
      enterDone: 'fade-enter-done',
      exit: 'fade-exit',
      exitActive: 'fade-exit-active',
      exitDone: 'fade-exit-done',
    });
  });
  it('should return only the relevant classNames if some of the animation modes are disabled', () => {
    expect(
      cssTransitionClassNames('fade', {
        appear: false,
        enter: false,
        exit: true,
      })
    ).toEqual({
      exit: 'fade-exit',
      exitActive: 'fade-exit-active',
      exitDone: 'fade-exit-done',
    });
  });
});
