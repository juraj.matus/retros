import { renderHook } from '@testing-library/react-hooks';

import { useMountAndUpdateEffect, useMountEffect } from './hookUtils';

describe('useMountEffect', () => {
  const res = {};
  res.render = function useHook() {
    return useMountEffect(() => {
      res.count++;
    });
  };

  beforeEach(() => {
    res.count = 0;
  });

  it('should be called on mount', () => {
    renderHook(res.render);
    expect(res.count).toBe(1);
  });
  it("shouldn't be called on update", () => {
    const { rerender } = renderHook(res.render);
    rerender();
    expect(res.count).toBe(1);
  });
});

describe('useMountAndUpdateEffect', () => {
  const res = {};
  res.render = function useHook({ i }) {
    return useMountAndUpdateEffect(
      {
        onMount() {
          res.mounted++;
        },
        onUpdate() {
          res.updated++;
        },
      },
      [i]
    );
  };

  beforeEach(() => {
    res.mounted = 0;
    res.updated = 0;
  });

  it('onMount should be called on mount', () => {
    renderHook(res.render, { initialProps: { i: 1 } });
    expect(res.mounted).toBe(1);
  });
  it("onMount shouldn't be called on update", () => {
    const { rerender } = renderHook(res.render, { initialProps: { i: 1 } });
    rerender({ i: 2 });
    expect(res.mounted).toBe(1);
  });
  it("onUpdate shouldn't be called on mount", () => {
    renderHook(res.render, { initialProps: { i: 1 } });
    expect(res.updated).toBe(0);
  });
  it("onUpdate shouldn't be called on non-dep-changing update", () => {
    const { rerender } = renderHook(res.render, { initialProps: { i: 1 } });
    rerender({ i: 1 });
    expect(res.updated).toBe(0);
  });
  it('onUpdate should be called on update', () => {
    const { rerender } = renderHook(res.render, { initialProps: { i: 1 } });
    rerender({ i: 2 });
    expect(res.updated).toBe(1);
  });
  it('onUpdate should be called on each update', () => {
    const { rerender } = renderHook(res.render, { initialProps: { i: 1 } });
    rerender({ i: 2 });
    rerender({ i: 3 });
    rerender({ i: 4 });
    expect(res.updated).toBe(3);
  });
});
