# Retros app

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Development

### Prerequisites

* nodejs

### Available scripts

Everything is configured in `package.json`

```sh
yarn start # Runs the app in the development mode (http://localhost:3000)
yarn test # Runs tests in CLI mode
yarn test:watch # Runs tests in interactive mode, reacting to changes
yarn build # Builds the project into the ./build directory
sh scripts/build-static.sh # Builds static html pages for better engine parsing. Not included in scripted build because it requires browser, which would make Docker too big
```
