# Injection of ENV variables
#   Inspired by https://manifold.co/blog/building-a-production-grade-container-for-your-static-javascript-application-b2b2eff83fbd

# Extracts variables from `.env` file into `$NGINX_SUB_FILTER` in format
# `sub_filter "NGINX_REPLACE_NAME" "${NAME}";`
export NGINX_SUB_FILTER=$(cat ./.env | grep '^REACT_APP' | sort | sed -e 's/REACT_APP_\([a-zA-Z_]*\)=\(.*\)/sub_filter\ \"NGINX_REPLACE_\1\" \"$\{\1\}\";/')
rm ./.env

# Replaces placeholder in nginx conf file by sub_filter directives
cat /etc/nginx/nginx.conf.template | \
  sed -e "s|LOCATION_SUB_FILTER|$(echo $NGINX_SUB_FILTER)|" | sed 's|}";\ |}";\n\t\t|g' > /etc/nginx/nginx.conf.template-env
rm /etc/nginx/nginx.conf.template
mv /etc/nginx/nginx.conf.template-env /etc/nginx/nginx.conf.template
