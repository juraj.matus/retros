# Replaces `${NAME}` placeholders in nginx conf file by the actual values
envsubst < /etc/nginx/nginx.conf.template > /etc/nginx/nginx.conf

nginx -g 'daemon off;'
