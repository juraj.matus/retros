# Extracts variables from `.env` file into `.env.local` in format
# `REACT_APP_NAME=NGINX_REPLACE_NAME`
cat ./.env | grep '^REACT_APP' | sort | sed -e 's|REACT_APP_\([a-zA-Z_]*\)=\(.*\)|REACT_APP_\1=NGINX_REPLACE_\1|' > ./.env.local
