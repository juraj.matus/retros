#!/bin/bash

set -e

JS_REGEXP='/static/js/main\.[[:alnum:]]*\.js'
CSS_REGEXP='/static/css/main\.[[:alnum:]]*\.css'

JS="$(grep -o "$JS_REGEXP" /build/index.html)"
CSS="$(grep -o "$CSS_REGEXP" /build/index.html)"

find /build.static -type f -print0 | xargs -r0 -n 1 sed -i 's|'"$JS_REGEXP"'|'"$JS"'|'
find /build.static -type f -print0 | xargs -r0 -n 1 sed -i 's|'"$CSS_REGEXP"'|'"$CSS"'|'
cp -r /build.static/* /build
