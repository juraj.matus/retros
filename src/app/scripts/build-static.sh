#!/bin/bash

set -e

cd "$(dirname $0)/.."

cp -r -T build build.orig
yarn build:static

rm -rf build.static
mkdir build.static
cp -r build/privacy-policy build/terms build.static

rm -rf build
mv -T build.orig build