// TODO - replace by `craco-less` when they upgrade it to work with CRA5
const CracoLessPlugin = require('craco-less-fix');

const { theme } = require('./src/styles/theme');

module.exports = {
  eslint: {
    enable: process.env.DISABLE_ESLINT !== 'true',
  },
  plugins: [
    {
      plugin: CracoLessPlugin,
      options: {
        lessLoaderOptions: {
          lessOptions: {
            sourceMap: true,
            javascriptEnabled: true,
            modifyVars: theme,
          },
        },
      },
    },
  ],
};
