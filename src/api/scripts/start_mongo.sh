#!/bin/bash

set -e

DOCKER_NAME="retros-mongo"
if docker ps -a | tail +2 | grep -q "$DOCKER_NAME"
then
  docker start "$DOCKER_NAME"
else
  docker run --name "$DOCKER_NAME" -p 27017:27017 mongo:5.0.5
fi