# Retros api

Express server using MongoDB as a data provider.

## Development

### Prerequisites

* nodejs
* docker

### Available scripts

Everything is configured in `package.json`

```sh
yarn start # Runs the server in the production mode
yarn start:dev # Runs the server in the development mode, including the database
yarn test # Runs tests in CLI mode
yarn test:watch # Runs tests in interactive mode, reacting to changes
```
