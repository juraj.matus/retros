import fs from 'fs';
import { dirname, join } from 'path';
import { fileURLToPath } from 'url';

import { load as loadYaml } from 'js-yaml';
import {
  compact,
  fromPairs,
  isNaN,
  isNil,
  isNumber,
  keys,
  mapValues,
  negate,
  pickBy,
  snakeCase,
} from 'lodash-es';

const DEFAULT_CONFIG = {
  port: 4000,
  apiDomain: '',
  appDomain: '',
  googleClientId: '',
  googleClientSecret: '',
  mongoUrl: '',
  trelloAppKey: '',
};

const FILENAME = fileURLToPath(import.meta.url);

function getEnvPath(env) {
  const inffices = {
    dev: '.development',
    prod: '.production',
  };
  return join(dirname(FILENAME), `env${inffices[env] || ''}.yaml`);
}

function loadExternalConfigs() {
  const envFiles = [getEnvPath(), getEnvPath(process.env.NODE_ENV)];
  const fileContents = compact(
    envFiles.map(path =>
      fs.existsSync(path) ? fs.readFileSync(path, { encoding: 'utf-8' }) : null
    )
  );
  const configs = fileContents.map(content => {
    try {
      return loadYaml(content);
    } catch (e) {
      return {};
    }
  });
  return configs.reduce((acc, cur) => ({ ...acc, ...cur }), {});
}

function envToNumber(val) {
  if (isNil(val)) {
    return null;
  }
  const num = parseInt(val, 10);
  return isNumber(num) && !isNaN(num) ? num : null;
}

function loadEnvConfig() {
  return fromPairs(
    keys(DEFAULT_CONFIG).map(key => [
      key,
      process.env[snakeCase(key).toUpperCase()],
    ])
  );
}

function mapLoadedConfig(config) {
  return pickBy(
    mapValues(config, (val, key) => {
      if (isNumber(DEFAULT_CONFIG[key])) {
        return envToNumber(val);
      }
      return val;
    }),
    negate(isNil)
  );
}

export const config = {
  ...DEFAULT_CONFIG,
  ...mapLoadedConfig(loadExternalConfigs()),
  ...mapLoadedConfig(loadEnvConfig()),
};

console.log(
  'Config: \n',
  mapValues(config, (val, key) =>
    key === 'mongoUrl' || /secret|key/i.test(key) ? '*****' : val
  )
);
