import express from 'express';
import { StatusCodes } from 'http-status-codes';
import passport from 'passport';

import { config } from './config.js';
import { rootRouter } from './routes/rootRouter.js';

function errorHandler(err, req, res, next) {
  console.log('Uncaught error: ', err);

  if (res.headersSent) {
    return next(err);
  }
  res.sendStatus(StatusCodes.INTERNAL_SERVER_ERROR);
}

export function makeServer() {
  const app = express();

  return new Promise(resolve => {
    app.use(passport.initialize());
    app.use(errorHandler);
    app.use('/', rootRouter);
    app.listen(config.port, () => {
      console.log(`Application running at port ${config.port}`);
      resolve(app);
    });
  });
}
