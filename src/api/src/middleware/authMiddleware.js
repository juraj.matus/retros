import StatusCodes from 'http-status-codes';

import { verifyGoogleOauth2Token } from '../core/auth.js';

export async function authenticatedMiddleware(req, res, next) {
  const accessToken = req?.headers?.['x-accesstoken'];
  const refreshToken = req?.headers?.['x-refreshtoken'];
  try {
    const { userId, email, newAccessToken, newRefreshToken } =
      await verifyGoogleOauth2Token({
        accessToken,
        refreshToken,
      });
    req.user = { id: userId, email };
    if (newAccessToken) {
      res.header('X-AccessToken', newAccessToken);
      res.header('X-RefreshToken', newRefreshToken);
    }
    next();
  } catch (e) {
    console.log('authenticatedMiddleware error', e);
    res.sendStatus(StatusCodes.UNAUTHORIZED);
  }
}
