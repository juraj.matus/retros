import { connectDb } from './db.js';
import { makeServer } from './server.js';

async function main() {
  await makeServer();
  await connectDb();
}
main();
