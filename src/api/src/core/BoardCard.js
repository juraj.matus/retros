import { values } from 'lodash-es';
import mongoose from 'mongoose';

import { BoardColumn } from './BoardColumn.js';

export const BoardCard = new mongoose.model(
  'BoardCard',
  new mongoose.Schema({
    boardId: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: 'Board',
    },
    column: {
      type: String,
      enum: values(BoardColumn),
    },
    contents: [
      new mongoose.Schema({
        ownerId: {
          type: String,
          required: true,
        },
        ownerDisplayName: {
          type: String,
          required: false,
        },
        ownerPicture: {
          type: String,
          required: false,
        },
        text: {
          type: String,
          required: true,
        },
        createdAt: {
          type: Date,
          required: true,
          default: Date.now,
        },
      }),
    ],
    votes: {
      type: [
        new mongoose.Schema({
          voter: {
            type: String,
            required: true,
          },
          value: {
            type: Number,
            required: true,
          },
        }),
      ],
      default: [],
    },
    trello: {
      type: new mongoose.Schema({
        cardId: {
          type: String,
        },
        fieldDescriptors: [
          new mongoose.Schema({
            fieldTitle: {
              type: String,
              required: true,
            },
            fieldName: {
              type: String,
              required: true,
            },
            fieldLabel: {
              type: String,
              required: true,
            },
            placeholder: {
              type: String,
            },
          }),
        ],
        includeVoteTally: {
          type: Boolean,
        },
        includeLink: {
          type: Boolean,
        },
      }),
      default: {},
    },
  })
);
