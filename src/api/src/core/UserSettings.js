import mongoose from 'mongoose';

export const UserSettings = new mongoose.model(
  'UserSettings',
  new mongoose.Schema({
    userId: {
      type: String,
      required: true,
    },
    trello: {
      type: new mongoose.Schema({
        token: {
          type: String,
        },
      }),
      default: {},
    },
  })
);
