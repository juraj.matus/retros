export const ErrorCode = {
  LEAVE_BOARD_OWNER: 'error.leaveBoard.owner',
};

export class AuthenticationError extends Error {
  constructor(message) {
    super(`Authentication error: ${message}`);
  }
}

export class BadInputError extends Error {}
