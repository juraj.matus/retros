import mongoose from 'mongoose';

export const BoardParticipant = new mongoose.model(
  'BoardParticipant',
  new mongoose.Schema({
    boardId: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: 'Board',
      required: true,
    },
    userId: {
      type: String,
      required: true,
    },
    displayName: {
      type: String,
      required: true,
    },
    picture: {
      type: String,
      required: false,
    },
    moderatorRight: {
      type: Boolean,
      required: true,
    },
  })
);
