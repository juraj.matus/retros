import axios from 'axios';
import { StatusCodes } from 'http-status-codes';
import passport from 'passport';
import { Strategy as GoogleStrategy } from 'passport-google-oauth20';
import passportOauth2Refresh from 'passport-oauth2-refresh';
import URI from 'urijs';

import { config } from '../config.js';
import { AuthenticationError } from './errors.js';

export function setupPassport() {
  const googleStrategy = new GoogleStrategy(
    {
      clientID: config.googleClientId,
      clientSecret: config.googleClientSecret,
      callbackURL: `${config.apiDomain}/auth/callback/google`,
      profileFields: ['id', 'displayName', 'email'],
    },
    async (accessToken, refreshToken, profile, callback) => {
      const userInfo = {
        id: profile.id,
        displayName: profile.displayName,
        email: profile._json.email,
        picture: profile._json.picture,
        accessToken,
        refreshToken,
      };
      callback(null, userInfo);
    }
  );

  passport.use(googleStrategy);
  passportOauth2Refresh.use(googleStrategy);

  passport.serializeUser((user, done) => {
    done(null, user);
  });

  passport.deserializeUser((user, done) => {
    done(null, user);
  });
}

async function requestNewToken(refreshToken) {
  console.log('Authentication: Refreshing token', refreshToken);
  const { newAccessToken, newRefreshToken } = await new Promise(
    (resolve, reject) =>
      passportOauth2Refresh.requestNewAccessToken(
        'google',
        refreshToken,
        (err, newAccessToken, newRefreshToken) => {
          console.log('Authentication: refreshToken result', {
            err,
            newAccessToken,
            newRefreshToken,
          });
          if (err) {
            reject(err);
          } else {
            resolve({
              newAccessToken,
              newRefreshToken: newRefreshToken || refreshToken,
            });
          }
        }
      )
  );
  return {
    ...(await verifyGoogleOauth2Token({
      accessToken: newAccessToken,
      refreshToken: newRefreshToken,
    })),
    newAccessToken,
    newRefreshToken,
  };
}

const GOOGLE_OAUTH2_TOKEN_INFO_URL = 'https://oauth2.googleapis.com/tokeninfo';
export async function verifyGoogleOauth2Token({ accessToken, refreshToken }) {
  if (!accessToken) {
    console.log('AuthenticationError: No token provided');
    throw new AuthenticationError('No token provided');
  }
  try {
    const { status, data } = await axios.get(
      URI(GOOGLE_OAUTH2_TOKEN_INFO_URL)
        .query({ access_token: accessToken })
        .toString()
    );
    if (status !== StatusCodes.OK) {
      console.log(`AuthenticationError: Invalid token (status: ${status})`);
      throw new AuthenticationError('Invalid token');
    }
    const { aud, sub: userId, email, exp, expires_in } = data;
    if (aud !== config.googleClientId) {
      console.log(`AuthenticationError: Invalid audience (${aud})`);
      throw new AuthenticationError('Invalid audience');
    }
    if (parseInt(expires_in, 10) < 600) {
      try {
        return await requestNewToken(refreshToken);
      } catch (e) {
        console.log(
          'AuthenticationWarning: Refresh token attempt unsuccessful'
        );
      }
    }
    if (new Date(exp * 1000) < new Date()) {
      try {
        return await requestNewToken(refreshToken);
      } catch (e) {
        console.log(
          `AuthenticationError: Expired (${new Date(exp * 1000).toISOString()})`
        );
        throw new AuthenticationError('Expired');
      }
    }
    return { userId, email };
  } catch (e) {
    if (e instanceof AuthenticationError) {
      throw e;
    }
    console.log('Authentication: Other error', e.message);
    throw new AuthenticationError('Verify token error');
  }
}
