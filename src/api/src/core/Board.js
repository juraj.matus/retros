import { values } from 'lodash-es';
import mongoose from 'mongoose';

import { BoardType } from './BoardType.js';

export const Board = new mongoose.model(
  'Board',
  new mongoose.Schema({
    ownerId: {
      type: String,
      required: true,
    },
    name: {
      type: String,
      required: true,
    },
    createdAt: {
      type: Date,
      required: true,
      default: Date.now,
    },
    type: {
      type: String,
      enum: values(BoardType),
    },
  })
);
