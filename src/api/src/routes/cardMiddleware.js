import asyncMiddleware from 'express-async-middleware';
import StatusCodes from 'http-status-codes';

import { BoardCard } from '../core/BoardCard.js';

export function boardCardInfoMiddleware({
  cardIdName = 'cardId',
  outputFieldName = 'cardInfo',
} = {}) {
  return asyncMiddleware(async (req, res, next) => {
    const cardId = req.params[cardIdName];

    const card = await BoardCard.findById(cardId);
    if (card === null) {
      res.status(StatusCodes.NOT_FOUND);
      res.send({ message: `Card '${cardId}' was not found` });
      return;
    }

    req[outputFieldName] = { card };
    next();
  });
}

export function boardCardContentGuardMiddleware({
  cardContentIdName = 'cardContentId',
  requireOwner,
}) {
  return asyncMiddleware(async (req, res, next) => {
    const { card } = req.cardInfo;

    const cardContentId = req.params[cardContentIdName];

    const content = (card.contents || []).find(
      ({ _id }) => _id.toString() === cardContentId
    );
    if (content === null) {
      res.status(StatusCodes.NOT_FOUND);
      res.send({
        message: `Card content '${cardContentId}' was not found in card '${card._id}'`,
      });
      return;
    }

    const isOwner = content.ownerId === req.user.id;

    if (requireOwner && !isOwner) {
      res.status(StatusCodes.FORBIDDEN);
      res.send({
        message: `User '${req.user.id}' is not an owner of card content '${cardContentId}'`,
      });
      return;
    }

    req.cardContentInfo = { content, isOwner };
    next();
  });
}
