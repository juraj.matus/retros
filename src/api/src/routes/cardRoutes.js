import { Router } from 'express';
import asyncHandler from 'express-async-handler';
import StatusCodes from 'http-status-codes';
import { isEmpty, sortBy, uniqBy } from 'lodash-es';

import { BoardCard } from '../core/BoardCard.js';
import { BoardParticipant } from '../core/BoardParticipant.js';
import { BadInputError } from '../core/errors.js';
import {
  boardCardContentGuardMiddleware,
  boardCardInfoMiddleware,
} from './cardMiddleware.js';
import { cardTrelloRouter } from './cardTrelloRoutes.js';

export const cardRouter = new Router();

const FORMATS = {
  csv: {
    mime: 'text/csv',
    formatter: data =>
      [
        'Column,Text,From,Date,Votes Up,Votes Down',
        ...data.map(card =>
          [
            card.column,
            card.text.replace(/"/g, '❞'),
            card.from,
            card.date.toISOString(),
            card.votes.up,
            card.votes.down,
          ].join(',')
        ),
      ].join('\n'),
  },
  json: { mime: 'application/json', formatter: data => data },
};
const DEFAULT_FORMAT = FORMATS.json;

function determineFormat(mime) {
  if (!mime) {
    return DEFAULT_FORMAT;
  }
  if (mime.endsWith('json')) {
    return FORMATS.json;
  }
  if (mime.endsWith('csv')) {
    return FORMATS.csv;
  }
  throw new BadInputError(`unexpected output mime ${mime}`);
}

cardRouter.get(
  '/export',
  asyncHandler(async (req, res) => {
    try {
      const format = determineFormat(req.header('Accept'));

      const { board } = req.boardInfo;
      const cards = await BoardCard.find({ boardId: board._id }).exec();
      const simpleCards = sortBy(
        cards
          .map(card => card.toObject())
          .map(
            card => ({
              column: card.column,
              text: card.contents.map(c => c.text).join('\n'),
              from: card.contents.map(c => c.ownerDisplayName).join('\n'),
              date: card.contents[0].createdAt,
              votes: {
                up: card.votes.filter(v => v.value === 1).length,
                down: card.votes.filter(v => v.value === -1).length,
              },
            }),
            card => card.votes.up - card.votes.down
          )
      );

      res.setHeader('Content-Type', format.mime);
      res.send(format.formatter(simpleCards));
    } catch (e) {
      console.log('export cards error', e);
      if (e instanceof BadInputError) {
        res.sendStatus(StatusCodes.BAD_REQUEST);
      } else {
        res.sendStatus(StatusCodes.INTERNAL_SERVER_ERROR);
      }
    }
  })
);

cardRouter.post(
  '/',
  asyncHandler(async (req, res) => {
    const { board } = req.boardInfo;

    const boardParticipant = await BoardParticipant.findOne({
      boardId: board._id,
      userId: req.user.id,
    }).select({ displayName: true, picture: true });

    const input = req.body;
    const card = new BoardCard({
      boardId: board._id,
      column: input.column,
      contents: [
        {
          ownerId: req.user.id,
          ownerDisplayName: boardParticipant.displayName,
          ownerPicture: boardParticipant.picture,
          text: input.text,
        },
      ],
    });
    try {
      const result = await card.save();
      res.send(result);
    } catch (e) {
      res.status(StatusCodes.BAD_REQUEST);
      res.send({ message: 'Validation error' });
    }
  })
);

cardRouter.post(
  '/merge/:card1Id/:card2Id',
  boardCardInfoMiddleware({
    cardIdName: 'card1Id',
    outputFieldName: 'card1Info',
  }),
  boardCardInfoMiddleware({
    cardIdName: 'card2Id',
    outputFieldName: 'card2Info',
  }),
  asyncHandler(async (req, res) => {
    const { board } = req.boardInfo;
    const { card: card1 } = req.card1Info;
    const { card: card2 } = req.card2Info;

    if (card1.column !== card2.column) {
      res.status(StatusCodes.BAD_REQUEST);
      res.send({ message: 'Validation error' });
      return;
    }

    const sortedVotes = sortBy([...card1.votes, ...card2.votes], vote =>
      Math.abs(vote.value)
    );
    const uniqueVotes = uniqBy(sortedVotes, 'voter');

    const card = new BoardCard({
      boardId: board._id,
      column: card1.column,
      contents: [...card1.contents, ...card2.contents],
      votes: uniqueVotes,
    });
    try {
      await card1.remove();
      await card2.remove();
      const result = await card.save();
      res.send(result);
    } catch (e) {
      res.status(StatusCodes.BAD_REQUEST);
      res.send({ message: 'Validation error' });
    }
  })
);

cardRouter.post(
  '/:cardId/vote',
  boardCardInfoMiddleware(),
  asyncHandler(async (req, res) => {
    const { card } = req.cardInfo;

    const { value } = req.body;
    const existingVote = card.votes.find(({ voter }) => voter === req.user.id);

    if (existingVote) {
      existingVote.value = value;
    } else {
      card.votes = [...card.votes, { voter: req.user.id, value }];
    }

    card.markModified('votes');
    try {
      const result = await card.save();
      res.send(result);
    } catch (e) {
      res.status(StatusCodes.BAD_REQUEST);
      res.send({ message: 'Validation error' });
    }
  })
);

cardRouter.use(
  '/:cardId/trello-card',
  boardCardInfoMiddleware(),
  cardTrelloRouter
);

cardRouter.put(
  '/:cardId/:cardContentId',
  boardCardInfoMiddleware(),
  boardCardContentGuardMiddleware({ requireOwner: true }),
  asyncHandler(async (req, res) => {
    const { card } = req.cardInfo;
    const { content } = req.cardContentInfo;

    const input = req.body;
    content.text = input.text;
    card.markModified('contents');
    try {
      const result = await card.save();
      res.send(result);
    } catch (e) {
      res.status(StatusCodes.BAD_REQUEST);
      res.send({ message: 'Validation error' });
    }
  })
);

cardRouter.delete(
  '/:cardId/:cardContentId',
  boardCardInfoMiddleware(),
  boardCardContentGuardMiddleware({ requireOwner: true }),
  asyncHandler(async (req, res) => {
    const { card } = req.cardInfo;
    const { content } = req.cardContentInfo;

    card.contents = card.contents.filter(c => c !== content);
    if (isEmpty(card.contents)) {
      await card.remove();
    } else {
      card.markModified('contents');
      await card.save();
    }
    res.sendStatus(StatusCodes.OK);
  })
);
