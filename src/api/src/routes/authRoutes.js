import { Router } from 'express';
import passport from 'passport';
import URI from 'urijs';

import { config } from '../config.js';
import { setupPassport } from '../core/auth.js';
import { authenticatedMiddleware } from '../middleware/authMiddleware.js';
import { userSettingsMiddleware } from './userSettingsMiddleware.js';

setupPassport();

export const authRouter = new Router();

function makeSettingsOutput(settings) {
  return {
    trelloEnabled: !!settings?.trello?.token,
  };
}

authRouter.get(
  '/settings',
  authenticatedMiddleware,
  userSettingsMiddleware(),
  (req, res) => {
    res.send(makeSettingsOutput(req.userSettings));
  }
);

authRouter.get(
  '/login/google',
  passport.authenticate('google', {
    scope: ['email', 'profile'],
    prompt: 'consent',
    accessType: 'offline',
  })
);

authRouter.get(
  '/callback/google',
  passport.authenticate('google', {
    failureRedirect: '/login',
    failureMessage: true,
  }),
  (req, res) => {
    res.redirect(
      URI(`${config.appDomain}/login/callback/`).query(req.user).toString()
    );
  },
  (err, req, res, next) => {
    if (err) {
      return res.redirect(
        URI(`${config.appDomain}/login/callback/`)
          .query({ error: err.message })
          .toString()
      );
    }
    next();
  }
);
