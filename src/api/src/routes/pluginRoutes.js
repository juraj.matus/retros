import { Router } from 'express';

import { trelloRouter } from './trelloRoutes.js';

export const pluginRouter = new Router();

pluginRouter.use('/trello', trelloRouter);
