import { Router } from 'express';

import { authRouter } from './authRoutes.js';
import { boardRouter } from './boardRoutes.js';
import { pluginRouter } from './pluginRoutes.js';

export const rootRouter = new Router();

rootRouter.use('/auth', authRouter);
rootRouter.use('/boards', boardRouter);
rootRouter.use('/plugins', pluginRouter);
