import asyncMiddleware from 'express-async-middleware';
import StatusCodes from 'http-status-codes';
import Trello from 'trello';

import { config } from '../config.js';

export function trelloMiddleware() {
  return asyncMiddleware(async (req, res, next) => {
    const { userSettings } = req;

    const token = userSettings?.trello?.token;
    if (!token) {
      res.status(StatusCodes.FORBIDDEN);
      res.send({ message: 'Trello is not connected' });
      return;
    }

    req.trello = new Trello(config.trelloAppKey, token);

    next();
  });
}

export async function handleTrelloError(res, handler) {
  try {
    await handler();
  } catch (e) {
    console.log('Trello error', e.message);
    if (e.message === 'invalid id') {
      res.sendStatus(StatusCodes.BAD_REQUEST);
    } else if (e.message.indexOf('not found') > -1) {
      res.sendStatus(StatusCodes.NOT_FOUND);
    } else if (e.message.indexOf('unauthorized') > -1) {
      res.sendStatus(StatusCodes.FORBIDDEN);
    } else {
      res.sendStatus(StatusCodes.INTERNAL_SERVER_ERROR);
    }
  }
}
