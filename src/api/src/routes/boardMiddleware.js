import asyncMiddleware from 'express-async-middleware';
import StatusCodes from 'http-status-codes';

import { Board } from '../core/Board.js';
import { BoardParticipant } from '../core/BoardParticipant.js';

export function boardInfoMiddleware({ boardIdName = 'id' } = {}) {
  return asyncMiddleware(async (req, res, next) => {
    const boardId = req.params[boardIdName];
    const userId = req.user.id;

    const board = await Board.findById(boardId);
    if (board === null) {
      res.status(StatusCodes.NOT_FOUND);
      res.send({ message: `Board '${boardId}' not found` });
      return;
    }

    const isOwner = board.ownerId === userId;

    req.boardInfo = { board, isOwner };
    next();
  });
}

export function boardGuardMiddleware({
  requireJoined,
  requireOwner,
  requireModerator,
}) {
  return asyncMiddleware(async (req, res, next) => {
    const { board, isOwner } = req.boardInfo;

    if (requireJoined) {
      const isJoined = await BoardParticipant.exists({
        boardId: board._id,
        userId: req.user.id,
      });
      if (!isJoined) {
        res.status(StatusCodes.FORBIDDEN);
        res.send({
          message: `User '${req.user.id}' is not a participant of board '${board._id}'`,
        });
        return;
      }
    }
    if (requireModerator) {
      const participant = await BoardParticipant.findOne({
        boardId: board._id,
        userId: req.user.id,
      }).select({ moderatorRight: true });
      if (!participant.moderatorRight) {
        res.status(StatusCodes.FORBIDDEN);
        res.send({
          message: `User '${req.user.id}' is not a moderator of board '${board._id}'`,
        });
        return;
      }
    }
    if (requireOwner && !isOwner) {
      res.status(StatusCodes.FORBIDDEN);
      res.send({
        message: `User '${req.user.id}' is not an owner of board '${board._id}'`,
      });
      return;
    }

    next();
  });
}
