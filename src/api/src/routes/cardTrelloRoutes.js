import { Router } from 'express';
import asyncHandler from 'express-async-handler';
import StatusCodes from 'http-status-codes';
import { compact, isEmpty, omit } from 'lodash-es';
import URI from 'urijs';

import { config } from '../config.js';
import { boardGuardMiddleware } from './boardMiddleware.js';
import { handleTrelloError, trelloMiddleware } from './trelloMiddleware.js';
import { userSettingsMiddleware } from './userSettingsMiddleware.js';

export const cardTrelloRouter = new Router();

cardTrelloRouter.use(userSettingsMiddleware(), trelloMiddleware());

function makeTrelloDescription({ input, card }) {
  const votesUp = card.votes.filter(vote => vote.value === 1).length;
  const votesDown = card.votes.filter(vote => vote.value === -1).length;

  const additions = compact([
    input.includeVoteTally && `🠝 ${votesUp} | 🠟 ${votesDown}`,
    input.includeLink &&
      `[source](${URI(config.appDomain)
        .path(`/board/${card.boardId.toString()}`)
        .query({ activeCard: card._id.toString() })})`,
  ]);

  return isEmpty(additions)
    ? input.description
    : `${input.description}\n---\n${additions.join(' | ')}`;
}

cardTrelloRouter.put(
  '/',
  boardGuardMiddleware({ requireModerator: true }),
  asyncHandler(async (req, res) => {
    const { card } = req.cardInfo;
    const input = req.body;

    const trelloCardId = card.trello?.cardId;

    await handleTrelloError(res, async () => {
      if (trelloCardId) {
        await req.trello.updateCardName(trelloCardId, input.title);
        await req.trello.updateCardDescription(
          trelloCardId,
          makeTrelloDescription({ input, card })
        );
        await req.trello.updateCard(
          trelloCardId,
          'idMembers',
          input.assigneeIds
        );
        await req.trello.updateCard(trelloCardId, 'idLabels', input.labelIds);
      } else {
        const trelloCard = await req.trello.addCardWithExtraParams(
          input.title,
          {
            desc: makeTrelloDescription({ input, card }),
            idMembers: input.assigneeIds,
            idLabels: input.labelIds,
          },
          input.listId
        );
        card.trello.cardId = trelloCard.id;
        card.trello.fieldDescriptors = input.fieldDescriptors;
      }
    });

    card.trello.includeVoteTally = input.includeVoteTally;
    card.trello.includeLink = input.includeLink;
    card.markModified('trello');
    await card.save();
    res.sendStatus(StatusCodes.OK);
  })
);

function makeCardOutput({ idBoard, idList, idLabels, idMembers, name, desc }) {
  return {
    boardId: idBoard,
    listId: idList,
    labelIds: idLabels,
    assigneeIds: idMembers,
    title: name,
    description: desc,
  };
}

cardTrelloRouter.get(
  '/',
  boardGuardMiddleware({ requireModerator: true }),
  asyncHandler(async (req, res) => {
    const { card } = req.cardInfo;

    await handleTrelloError(res, async () => {
      if (card.trello?.cardId) {
        res.send({
          ...makeCardOutput(await req.trello.getCardById(card.trello.cardId)),
          ...omit(card.trello.toObject(), ['cardId']),
        });
      } else {
        res.send(null);
      }
    });
  })
);

cardTrelloRouter.delete(
  '/',
  boardGuardMiddleware({ requireModerator: true }),
  asyncHandler(async (req, res) => {
    const { card } = req.cardInfo;
    card.trello.cardId = null;
    card.markModified('trello');
    await card.save();
    res.sendStatus(StatusCodes.OK);
  })
);
