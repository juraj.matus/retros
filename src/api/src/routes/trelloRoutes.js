import express, { Router } from 'express';
import asyncHandler from 'express-async-handler';
import StatusCodes from 'http-status-codes';
import { property } from 'lodash-es';
import URI from 'urijs';

import { config } from '../config.js';
import { authenticatedMiddleware } from '../middleware/authMiddleware.js';
import { handleTrelloError, trelloMiddleware } from './trelloMiddleware.js';
import { userSettingsMiddleware } from './userSettingsMiddleware.js';

export const trelloRouter = new Router();

trelloRouter.use(express.json());

const TRELLO_AUTHORIZE_URL = 'https://trello.com/1/authorize';
const TRELLO_AUTHORIZE_QUERY_PARAMS = {
  expiration: 'never',
  scope: ['read', 'write', 'account'].join(','),
  name: 'Retros.Instea',
};

trelloRouter.get('/authorize', (req, res) => {
  res.redirect(
    URI(TRELLO_AUTHORIZE_URL)
      .query({
        ...TRELLO_AUTHORIZE_QUERY_PARAMS,
        key: config.trelloAppKey,
        return_url: URI(`${config.appDomain}/trello/callback`)
          .query({ redirect: req.query.redirect })
          .toString(),
      })
      .toString()
  );
});

trelloRouter.put(
  '/token',
  authenticatedMiddleware,
  userSettingsMiddleware(),
  asyncHandler(async (req, res) => {
    const { userSettings } = req;
    const input = req.body;
    userSettings.trello.token = input.token || null;
    userSettings.markModified('trello');
    await userSettings.save();

    res.sendStatus(StatusCodes.OK);
  })
);

const protectedTrelloRouter = new Router();
trelloRouter.use('/api', protectedTrelloRouter);
protectedTrelloRouter.use(
  authenticatedMiddleware,
  userSettingsMiddleware(),
  trelloMiddleware()
);

protectedTrelloRouter.get(
  '/boards',
  asyncHandler(async (req, res) => {
    await handleTrelloError(res, async () => {
      const boards = (await req.trello.getBoards('me')).map(({ id, name }) => ({
        id,
        name,
      }));
      res.send(boards);
    });
  })
);

protectedTrelloRouter.get(
  '/boards/:id/lists',
  asyncHandler(async (req, res) => {
    await handleTrelloError(res, async () => {
      const lists = (
        await req.trello.getListsOnBoard(req.params.id, 'id,name')
      ).map(({ id, name }) => ({
        id,
        name,
      }));
      res.send(lists);
    });
  })
);

protectedTrelloRouter.get(
  '/boards/:id/labels',
  asyncHandler(async (req, res) => {
    await handleTrelloError(res, async () => {
      const labels = (await req.trello.getLabelsForBoard(req.params.id)).map(
        ({ id, name, color }) => ({
          id,
          name: name || color,
        })
      );
      res.send(labels);
    });
  })
);

protectedTrelloRouter.get(
  '/boards/:id/members',
  asyncHandler(async (req, res) => {
    await handleTrelloError(res, async () => {
      const members = (await req.trello.getBoardMembers(req.params.id)).map(
        ({ id, fullName }) => ({
          id,
          name: fullName,
        })
      );
      res.send(members);
    });
  })
);

protectedTrelloRouter.get(
  '/boards/:id/templates',
  asyncHandler(async (req, res) => {
    await handleTrelloError(res, async () => {
      const cards = (await req.trello.getCardsOnBoard(req.params.id))
        .filter(property('isTemplate'))
        .map(({ id, name, desc }) => ({ id, name, desc }));
      res.send(cards);
    });
  })
);
