import express, { Router } from 'express';
import asyncHandler from 'express-async-handler';
import { StatusCodes } from 'http-status-codes';
import { compact, isEmpty, pick, property } from 'lodash-es';

import { Board } from '../core/Board.js';
import { BoardCard } from '../core/BoardCard.js';
import { BoardParticipant } from '../core/BoardParticipant.js';
import { ErrorCode } from '../core/errors.js';
import { authenticatedMiddleware } from '../middleware/authMiddleware.js';
import {
  boardGuardMiddleware,
  boardInfoMiddleware,
} from './boardMiddleware.js';
import { cardRouter } from './cardRoutes.js';
import { trelloMiddleware } from './trelloMiddleware.js';
import { userSettingsMiddleware } from './userSettingsMiddleware.js';

export const boardRouter = new Router();

boardRouter.use(authenticatedMiddleware);
boardRouter.use(express.json());

function makeBoardOutput({ board, participants, cards, user }) {
  return {
    ...board.toObject(),
    participants: participants.map(participant => ({
      ...participant.toObject(),
      ownerRight: participant.userId === board.ownerId,
    })),
    cards: cards.map(card => {
      const { _id, column, contents, votes } = card.toObject();
      return {
        _id,
        column,
        contents: contents.map(c => ({
          ...c,
          isOwner: user.id === c.ownerId,
        })),
        votes: {
          up: votes.filter(v => v.value === 1).length,
          down: votes.filter(v => v.value === -1).length,
          my: votes.find(v => v.voter === user.id)?.value || 0,
        },
      };
    }),
  };
}

boardRouter.post(
  '/',
  asyncHandler(async (req, res) => {
    const input = req.body;
    const board = new Board({
      type: input.type,
      name: input.name,
      ownerId: req.user.id,
    });
    try {
      const result = await board.save();
      res.send(result);
    } catch (e) {
      res.status(StatusCodes.BAD_REQUEST);
      res.send({ message: 'Validation error' });
    }
  })
);

boardRouter.get(
  '/',
  asyncHandler(async (req, res) => {
    const boardIds = (
      await BoardParticipant.find({ userId: req.user.id })
        .select({ boardId: true })
        .exec()
    ).map(property('boardId'));
    const boards = await Board.find({ _id: { $in: boardIds } })
      .sort([['createdAt', -1]])
      .exec();
    res.send(boards);
  })
);

boardRouter.put(
  '/:id',
  boardInfoMiddleware(),
  boardGuardMiddleware({ requireOwner: true }),
  asyncHandler(async (req, res) => {
    const { board } = req.boardInfo;
    const input = req.body;
    board.name = input.name;
    try {
      const result = await board.save();
      res.send(result);
    } catch (e) {
      res.status(StatusCodes.BAD_REQUEST);
      res.send({ message: 'Validation error' });
    }
  })
);

boardRouter.delete(
  '/:id',
  boardInfoMiddleware(),
  boardGuardMiddleware({ requireOwner: true }),
  asyncHandler(async (req, res) => {
    const { board } = req.boardInfo;
    await Promise.all([
      BoardParticipant.deleteMany({ boardId: board._id }),
      BoardCard.deleteMany({ boardId: board._id }),
      board.remove(),
    ]);
    res.sendStatus(StatusCodes.OK);
  })
);

boardRouter.get(
  '/:id',
  boardInfoMiddleware(),
  boardGuardMiddleware({ requireJoined: true }),
  asyncHandler(async (req, res) => {
    const { board } = req.boardInfo;

    const participants = await BoardParticipant.find({
      boardId: board._id,
    }).exec();

    const cards = await BoardCard.find({ boardId: board._id }).exec();

    res.send(makeBoardOutput({ board, participants, cards, user: req.user }));
  })
);

boardRouter.post(
  '/:id/join',
  boardInfoMiddleware(),
  asyncHandler(async (req, res) => {
    const { board, isOwner } = req.boardInfo;

    const existent = await BoardParticipant.findOne({
      boardId: board._id,
      userId: req.user.id,
    }).select({ moderatorRight: true });
    if (existent) {
      res.sendStatus(StatusCodes.OK);
      return;
    }

    const input = req.body;
    const boardParticipant = new BoardParticipant({
      boardId: board._id,
      userId: req.user.id,
      displayName: input.displayName,
      picture: input.picture,
      moderatorRight: isOwner,
    });
    try {
      await boardParticipant.save();

      await BoardCard.updateMany(
        { boardId: board._id },
        {
          $set: {
            'contents.$[content].ownerDisplayName': input.displayName,
            'contents.$[content].ownerPicture': input.picture,
          },
        },
        { arrayFilters: [{ 'content.ownerId': req.user.id }] }
      );

      res.sendStatus(StatusCodes.OK);
    } catch (e) {
      res.status(StatusCodes.BAD_REQUEST);
      res.send({ message: 'Validation error' });
    }
  })
);

boardRouter.post(
  '/:id/leave',
  boardInfoMiddleware(),
  asyncHandler(async (req, res) => {
    const { board } = req.boardInfo;

    if (req.user.id === board.ownerId) {
      res.status(StatusCodes.BAD_REQUEST);
      res.send({ code: ErrorCode.LEAVE_BOARD_OWNER });
      return;
    }

    await BoardParticipant.deleteOne({
      boardId: board._id,
      userId: req.user.id,
    });

    await BoardCard.updateMany(
      { boardId: board._id },
      {
        $set: {
          'contents.$[content].ownerDisplayName': null,
          'contents.$[content].ownerPicture': null,
        },
      },
      { arrayFilters: [{ 'content.ownerId': req.user.id }] }
    );

    res.sendStatus(StatusCodes.OK);
  })
);

boardRouter.post(
  '/:id/participants/:userId/rights',
  boardInfoMiddleware(),
  boardGuardMiddleware({ requireOwner: true }),
  asyncHandler(async (req, res) => {
    const { board } = req.boardInfo;

    const input = pick(req.body, ['moderatorRight']);
    if (isEmpty(input)) {
      res.sendStatus(StatusCodes.OK);
      return;
    }

    await BoardParticipant.updateOne(
      {
        boardId: board._id,
        userId: req.params.userId,
      },
      { $set: input }
    );
    res.sendStatus(StatusCodes.OK);
  })
);

boardRouter.get(
  '/:id/trello-output',
  boardInfoMiddleware(),
  boardGuardMiddleware({ requireJoined: true }),
  userSettingsMiddleware(),
  trelloMiddleware(),
  asyncHandler(async (req, res) => {
    const { board } = req.boardInfo;

    const cards = await BoardCard.find({ boardId: board._id }).exec();

    const trelloCardInfos = cards
      .filter(property('trello.cardId'))
      .map(property('trello'));

    const trelloCards = compact(
      await Promise.all(
        trelloCardInfos.map(async ({ cardId }) => {
          try {
            const { idMembers, name, url } = await req.trello.getCardById(
              cardId
            );
            const members = await Promise.all(
              idMembers.map(async id => await req.trello.getMember(id))
            );
            return {
              title: name,
              url,
              members: members.map(property('fullName')),
            };
          } catch (e) {
            return null;
          }
        })
      )
    );

    res.send(
      trelloCards
        .map(
          ({ title, url, members }) =>
            `${members.map(m => `@${m}`).join(', ')}: ${title} ([link](${url}))`
        )
        .join('\n\n')
    );
  })
);

boardRouter.use(
  '/:id/cards',
  boardInfoMiddleware(),
  boardGuardMiddleware({ requireJoined: true }),
  cardRouter
);
