import asyncMiddleware from 'express-async-middleware';

import { UserSettings } from '../core/UserSettings.js';

export function userSettingsMiddleware() {
  return asyncMiddleware(async (req, res, next) => {
    const userId = req.user.id;

    const oldSettings = await UserSettings.findOne({ userId });
    const settings = oldSettings || (await new UserSettings({ userId }).save());

    req.userSettings = settings;
    next();
  });
}
