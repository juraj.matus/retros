/* eslint-disable import/no-unused-modules */
export default {
  testEnvironment: 'node',
  transform: {
    '^.+\\.(js|jsx)$': 'babel-jest',
  },
  setupFilesAfterEnv: ['@alex_neo/jest-expect-message'], // FIXME: https://github.com/mattphillips/jest-expect-message/pull/40
  reporters: [
    'default',
    [
      'jest-junit',
      {
        suiteName: 'Vifzack API - Unit tests',
        outputName: 'reports/test-report.xml',
        outputDirectory: '.',
        uniqueOutputName: 'false',
        classNameTemplate: 'API - {classname}',
        suiteNameTemplate: 'API - {title}',
        titleTemplate: '{title}',
        ancestorSeparator: ' › ',
        includeShortConsoleOutput: 'true',
      },
    ],
  ],
};
