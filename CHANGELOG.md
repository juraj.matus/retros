# V1.0.1

* More compact design
* Fullscreen mode
* Configurable sorting

# V1.0.2

* Trello connect error handling

# V1.0.3

* More board types
* Fix of dialogs in fullscreen mode
* Better responsive design

# V1.1.0

* Fix of "undefined" literals in Trello for empty fields
* Possibility to export board cards into a compact format

# V1.1.3

* New board type
