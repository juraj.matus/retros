#!/bin/bash

set -e

cd $(dirname $0)/..

MONGO_URL=mongodb://retros-user:gP3tLXte9Q7rgixB@mongo:27017/retros \
 API_DOMAIN=http://localhost:3060 \
 APP_DOMAIN=http://localhost:3060 \
 docker-compose -f docker/docker-compose-deploy.yml -p retros up "$@"
