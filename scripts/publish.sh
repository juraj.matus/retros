#!/bin/bash

# ENV inputs:
#  BUMP_APP_VERSION_TYPE = none|patch|minor|major
#  BUMP_API_VERSION_TYPE = none|patch|minor|major

set -e

BUMP_APP_VERSION_TYPE="${BUMP_APP_VERSION_TYPE:-patch}"
BUMP_API_VERSION_TYPE="${BUMP_API_VERSION_TYPE:-patch}"
REGISTRY="registry.gitlab.com/juraj.matus/retros"

cd $(dirname $0)/..

# tag_and_push $folder $tag-infix $docker-compose-name $bump-version-type
tag_and_push() {
  FOLDER="$1"
  BUMP_VERSION_TYPE="$4"
  pushd "$FOLDER" &>/dev/null
    if grep -E -q "^(patch|minor|major)$" <<< "$BUMP_VERSION_TYPE" ; then
      npm version "$BUMP_VERSION_TYPE" 1>&2
    fi
    VERSION=$(npm run get-version | tail -n 1)
  popd &>/dev/null
  DOCKER_TAG="${REGISTRY}/"$2":$VERSION"
  DOCKER_COMPOSE_NAME="$3"

  DOCKER_ID=$(docker image ls | grep "$DOCKER_COMPOSE_NAME" | awk '{ print $3 }')
  if [ -n "$DOCKER_ID" ] ; then
    docker tag "$DOCKER_ID" "$DOCKER_TAG" 1>&2
    docker push "$DOCKER_TAG" 1>&2
  fi

  echo "$DOCKER_TAG"
  echo "$VERSION"
}

API_INFO=($(tag_and_push "src/api" "api" "retros-api" "$BUMP_API_VERSION_TYPE"))
API_IMAGE="${API_INFO[0]}"
API_VERSION="${API_INFO[1]}"
APP_INFO=($(tag_and_push "src/app" "app" "retros-app" "$BUMP_APP_VERSION_TYPE"))
APP_IMAGE="${APP_INFO[0]}"
APP_VERSION="${APP_INFO[1]}"

grep -v 'build' docker/docker-compose.yml \
 | grep -v 'context' \
 | grep -v 'dockerfile' \
 | sed 's|image: retros-api|'"image: $API_IMAGE"'|' \
 | sed 's|image: retros-app|'"image: $APP_IMAGE"'|' \
 | sed 's|VERSION: app-version|'"VERSION: $APP_VERSION"'|' \
 > docker/docker-compose-deploy.yml

CHANGED_FILES=($(git status | grep -E '(package\.json|docker-compose-deploy\.yml)$' | awk '{ print $2 }'))
if [ "${#CHANGED_FILES[@]}" -gt 0 ] ; then
  git add "${CHANGED_FILES[@]}"
  git commit -m "New version published"
  git push --no-verify
fi
