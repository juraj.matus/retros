#!/bin/bash

set -e

cd $(dirname $0)/..

for rep in src/{app,api}/reports ; do
  if [ -d "$rep" ] ; then
    rm -rf "$rep"
  fi
done

DOCKER_BUILDKIT=0 docker-compose -f docker/docker-compose.yml -p retros build "$@"

DOCKER_ID_APP=$(docker-compose -f docker/docker-compose.yml -p retros ps -q retros-app 2>/dev/null)
if [ -n "$DOCKER_ID_APP" ] ; then
  echo "Collecting app test reports"
  rm -rf src/app/reports/
  docker cp "$DOCKER_ID":/reports src/app
fi

DOCKER_ID_API=$(docker-compose -f docker/docker-compose.yml -p retros ps -q retros-api 2>/dev/null)
if [ -n "$DOCKER_ID_API" ] ; then
  echo "Collecting api test reports"
  rm -rf src/api/reports/
  docker cp "$DOCKER_ID":/reports src/api
fi
